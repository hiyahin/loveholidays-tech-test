var assert = require('assert');
global.window = {};

var LH = require('../LH.js');
describe('LH', function() {
	afterEach(function() {
		LH.clearFeatures();
	});

	describe('push', function() {
  	it('should push a feature to its array', function (){
  		var feature = 'AwesomeFeature';
  		LH.push(feature);

  		assert.equal(LH.enabledFeatures.indexOf(feature) != -1, true);
  	})
  	it('should push a string to its array', function (){
  		var feature = 123;
  		LH.push(feature);

  		assert.equal(LH.enabledFeatures.indexOf(feature) != -1, false);
  	})
	});

  describe('getEnabledIds', function() {
  	it('should return empty string if no enabled features', function (){
  		assert.equal(LH.getEnabledIds(), '');
  	})

  	it('should return string of enabled feature', function (){
  		var feature = 'sortByPrice';
  		LH.push(feature);
  		assert.equal(LH.getEnabledIds(), 'A');
  	})

  	it('should return string of multiple enabled features', function (){
  		var feature = 'sortByPrice';
  		LH.push(feature);
  		var feature = 'showRecentlyViewed';
  		LH.push(feature);
  		assert.equal(LH.getEnabledIds(), 'AF');
  	})
  });

  describe('isEnabled', function() {
  	it('should return false for features that are not enabled', function (){
  		var feature = 'showRecentlyViewed';
  		LH.push(feature);
  		var feature2 = 'sortByPrice';
  		assert.equal(LH.isEnabled(feature2), false);
  	})

  	it('should return true for features that are not enabled', function (){
  		var feature = 'showRecentlyViewed';
  		LH.push(feature);
  		var feature2 = 'showRecentlyViewed';
  		assert.equal(LH.isEnabled(feature2), true);
  	})
  });
});