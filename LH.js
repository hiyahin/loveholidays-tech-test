var LH = window.LH || {};
LH.enabledFeatures = [];
LH.flags = {
	sortByPrice: 'A',
	showRecentlyViewed: 'F'
};
//Need function for adding more flags?


LH.push = function (feature) {
	if (typeof feature == "string") {
		LH.enabledFeatures.push(feature);
	}
	//Need error handling?
}

LH.getEnabledIds = function () {
	return LH.enabledFeatures
		.map(function(feature){
			return LH.flags[feature] !== undefined ? LH.flags[feature] : null
		})
		.join('');
}

LH.isEnabled = function (feature) {
	return LH.enabledFeatures.indexOf(feature) !== -1
}

//For testing
LH.clearFeatures = function() {
	LH.enabledFeatures = [];
}

//Allow to run as browser script or required in
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
  module.exports = LH;
}
else {
  window.LH = LH;
}
